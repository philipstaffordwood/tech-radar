export type RingType = 'ADOPT' | 'TRIAL' | 'ASSESS' | 'HOLD' | 'RISK';
